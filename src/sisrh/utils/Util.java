package sisrh.utils;

import java.util.Random;

public class Util {

    public static boolean validarCpf(String cpf) {
        // Posso aqui implementar o cálculo e comparação dos dígitos verificadores do CPF
        // passado e somente retornar "true" se "baterem"
        return true;
    }

    public static String gerarSenha() {
        return gerarSenha(4);
    }

    public static String gerarSenha(int tamanho) {
        String senha = "";
        Random random = new Random();
        
        for (int i = 0; i < tamanho; i++)
            senha = senha + random.nextInt(10);
        
        return senha;
    }

    public static String gerarSenha(int tamanho, boolean letras) {
        
        if (!letras) {
            return gerarSenha(tamanho);
        } else {
            String senha = "";
            Random random = new Random();

            for (int i = 0; i < tamanho; i++) {
                int numero = random.nextInt(26) + 97;
                senha = senha + String.valueOf((char)numero);
            }
            return senha;
        }
    }

}