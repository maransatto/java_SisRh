package sisrh.visao;

import javax.swing.JOptionPane;
import sisrh.modelo.Funcionario;
import sisrh.modelo.Gerente;


public class Principal {

    public static void main(String[] args) {
        Funcionario f1 = new Funcionario();
        // JOptionPane.showMessageDialog(null, f1);

        Funcionario f2 = new Funcionario();
        f2.setCpf("354.295.738-09");
        f2.setNome("Maransatto");
        f2.setSalario(500);
        // JOptionPane.showMessageDialog(null, f2);

        Gerente g1 = new Gerente();
        JOptionPane.showMessageDialog(null, g1);
    }
}