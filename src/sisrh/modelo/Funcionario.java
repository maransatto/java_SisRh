package sisrh.modelo;

import java.security.Principal;
import java.text.DecimalFormat;

import sisrh.utils.Util;


public class Funcionario {

    private int codigo;
    private String nome;
    private String cpf;
    private double salario;

    protected String parceira;

    private static int contador = 0;

    public Funcionario() {
        contador++;
        this.codigo = contador;
        this.nome = "ANÔNIMO";
        this.cpf = "NÃO INFORMADO";
        this.salario = 1065;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getNome() {
        return this.nome.toUpperCase();
    }

    public void setNome(String nome) {
        // Só não vou setar o nome do funcionário se o nome passado NÃO (!) for vazio.
        if (!nome.isEmpty()) {
            this.nome = nome;
        }
    }
    
    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        if (Util.validarCpf(cpf)) {
            this.cpf = cpf;
        }
    }

    public String getSalario() {
        String salarioFormatado = DecimalFormat.
                getCurrencyInstance().format(this.salario);
        return salarioFormatado;
    }

    public void setSalario(double salario) {
        // De acordo com a CLT, um funcionário não pode (ainda) ter seu salário reduzido
        if (salario > this.salario) {
            this.salario = salario;
        }
    }

    public String toString() {
        return "CÓDIGO: " + getCodigo() + "\nNOME: " + getNome() +
                "\nCPF: " + getCpf() + "\nSALÁRIO: " + getSalario();
    }
    
}
