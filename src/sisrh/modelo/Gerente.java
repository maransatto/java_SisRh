package sisrh.modelo;

import sisrh.utils.Util;

/**
 * Gerente
 */
public class Gerente extends Funcionario {

    private String setor;
    private String senha;

    public Gerente() {
        super();
        this.setor = "VENDAS";
        this.senha = Util.gerarSenha(8, true);
    }

    public String getSetor() {
        return this.setor.toUpperCase();
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getSenha() {
        return this.senha;
    }

    public void setSenha() {
        this.senha = Util.gerarSenha();
    }

    public String toString() {
        return super.toString() + "\nSETOR: " + this.getSetor() +
                "\nSENHA: " + this.getSenha();
    }
}